package com.blueraptor.rabbitmqdemo.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.security.SecureRandomParameters;
import java.util.Objects;

@Entity
public class Message implements Serializable {
    @Id
    @GeneratedValue
    private Integer Id;

    @Column
    private String message;

    @Column
    private String source;

    @Column
    private String targetQ;

    public Message(String message, String source, String targetQ) {
        this.message = message;
        this.source = source;
        this.targetQ = targetQ;
    }

    protected  Message(){

    }

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getTargetQ() {
        return targetQ;
    }

    public void setTargetQ(String targetQ) {
        this.targetQ = targetQ;
    }

    @Override
    public String toString() {
        return "Message{" +
                "Id=" + Id +
                ", message='" + message + '\'' +
                ", source='" + source + '\'' +
                ", targetQ='" + targetQ + '\'' +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message1 = (Message) o;
        return Objects.equals(Id, message1.Id) &&
                Objects.equals(message, message1.message) &&
                Objects.equals(source, message1.source) &&
                Objects.equals(targetQ, message1.targetQ);
    }

    @Override
    public int hashCode() {
        return Objects.hash(Id, message, source, targetQ);
    }
}
