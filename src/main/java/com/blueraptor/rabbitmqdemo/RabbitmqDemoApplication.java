package com.blueraptor.rabbitmqdemo;

import com.blueraptor.rabbitmqdemo.service.ReceiveService;
import com.blueraptor.rabbitmqdemo.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Profile;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
@Profile("send")
public class RabbitmqDemoApplication {
    @Autowired
    private SendService sendService;
    @Autowired
    private ReceiveService receiveService;

    public static void main(String[] args) throws IOException, TimeoutException {
        SpringApplication.run(RabbitmqDemoApplication.class, args);
    }

}
