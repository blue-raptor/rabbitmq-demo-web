package com.blueraptor.rabbitmqdemo.controller;

import com.blueraptor.rabbitmqdemo.domain.Message;
import com.blueraptor.rabbitmqdemo.service.SendService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@RestController
public class PublishController {
    SendService senderService;

    @Autowired
    public PublishController(SendService sendService) {
        this.senderService = sendService;
    }

    @RequestMapping(value = "/send", method = RequestMethod.POST)
    public void sendMessage() throws IOException, TimeoutException {
        Message msg = this.senderService.createMessage("Repository Message","New Sender", "demo-q");
        this.senderService.fire(msg);
    }
}
